import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PostComment } from 'src/entities';
import { CommandManager } from 'src/entities';
import { Repository } from 'typeorm';
import { CreatePostCommentDto, UpdatePostCommentDto } from './dtos/postComment.dto';

@Injectable()
export class PostCommentService {
  private readonly postCommentCommand: CommandManager<PostComment>;

  public constructor(
    @InjectRepository(PostComment)
    private readonly postCommentRepository: Repository<PostComment>,
  ) {
    this.postCommentCommand = CommandManager(this.postCommentRepository);
  }

  async createPostComment(userId: number, postId: number, data: CreatePostCommentDto) {
    const postComment = await this.postCommentCommand.createOne({ userId, postId, ...data });
    return postComment;
  }

  async updatePostComment(postCommentId: number, data: UpdatePostCommentDto) {
    await this.postCommentCommand.updateOne(postCommentId, { id: postCommentId, ...data });
  }

  async softDeletePostComment(postCommentId: number) {
    await this.postCommentCommand.softDeleteOne(postCommentId);
  }
}
