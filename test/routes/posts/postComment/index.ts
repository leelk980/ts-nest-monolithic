import createOneTest from './create-one.spec';
import deleteOneTest from './delete-one.spec';
import updateOneTest from './update-one.spec';

const PostLikeTest = () => {
  describe('[POST] /api/posts/:postId/comments => createOneTest', createOneTest);
  describe('[PUT] /api/posts/:postId/comments/:id => updateOneTest', updateOneTest);
  describe('[DELETE] /api/posts/:postId/comments/:id => deleteOneTest', deleteOneTest);
};

export default PostLikeTest;
