import { Repository, SelectQueryBuilder } from 'typeorm';
import { QueryManager } from './@query.manager';
import { Post } from '../index';

export type PostQueryManager = ReturnType<typeof PostQueryManager>;
export const PostQueryManager = (repository: Repository<Post>) => {
  const build = function () {
    const entityName = 'post';
    const qb = repository.createQueryBuilder(entityName) as SelectQueryBuilder<Post> &
      typeof Wheres;

    /** Columns */
    const id = (value: number) => {
      return qb.andWhere(`${entityName}.id = :value`, { value });
    };

    const title = (value: string) => {
      return qb.andWhere(`${entityName}.title = :value`, { value });
    };

    const content = (value: string) => {
      return qb.andWhere(`${entityName}.content = :value`, { value });
    };

    const Wheres = { id, title, content };
    for (const key in Wheres) {
      qb[key] = Wheres[key];
    }

    return qb;
  };

  const manager = QueryManager(repository);

  return { build, manager };
};
