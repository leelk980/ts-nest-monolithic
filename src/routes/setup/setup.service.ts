import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category, CategoryNameEnum } from 'src/entities';
import { CommandManager } from 'src/entities';
import { TreeRepository } from 'typeorm';

@Injectable()
export class SetupService {
  public categoryCommand: CommandManager<Category>;

  constructor(
    @InjectRepository(Category)
    private readonly categoryRepository: TreeRepository<Category>,
  ) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore https://github.com/nestjs/nest/issues/2834
    this.categoryCommand = CommandManager(this.categoryRepository);
  }

  async setupCategories() {
    await this.categoryCommand.hardDeleteMany({});

    const parents = {
      0: null,
      1: null,

      2: 0,
      3: 0,
      4: 0,
      5: 1,
      6: 1,
      7: 1,

      8: 2,
      9: 2,
      10: 3,
      11: 3,
      12: 4,
      13: 4,
      14: 5,
      15: 5,
      16: 6,
      17: 6,
      18: 7,
      19: 7,
    };

    const categories: Category[] = [];
    const enumKeys = Object.keys(CategoryNameEnum);

    /** for const loop로 index 받는 방법 */
    for await (const [index, enumKey] of enumKeys.entries()) {
      const category = await this.categoryCommand.createOne({
        name: CategoryNameEnum[enumKey],
        description: `${CategoryNameEnum[enumKey]} 관련 상세설명`,
        parent: parents[index] >= 0 ? categories[parents[index]] : null,
      });

      categories.push(category);
    }

    return categories;
  }

  /** Queries */
  async findCategoryTrees() {
    const categories = await this.categoryRepository.findTrees();
    return categories;
  }
}
