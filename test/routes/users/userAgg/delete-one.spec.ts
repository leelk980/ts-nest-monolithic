import { BaseErrorOutput } from 'src/common';
import { Post } from 'src/entities';
import { UserOutput } from 'src/routes/users/userAgg';
import { createRandomPost, createRandomUser, RandomUser } from 'test/@utils';
import { HyperTest, initHyperTest } from 'test/hyperTest';

const tests = () => {
  let randomUser: RandomUser;
  let userPost: Post;
  let hyperTest: HyperTest;
  beforeAll(async () => {
    randomUser = await createRandomUser();
    userPost = await createRandomPost(randomUser.accessToken);
    hyperTest = await initHyperTest();
  });

  it('should fail to delete another user', async () => {
    const anotherUser = await createRandomUser();

    const before = await hyperTest
      .get<UserOutput>(`/api/users/${anotherUser.info.id}`)
      .authToken(global.adminToken)
      .query();

    expect(before.isSuccess).toEqual(true);
    expect(before.user.id).toEqual(anotherUser.info.id);

    const res = await hyperTest
      .delete<BaseErrorOutput>(`/api/users/${anotherUser.info.id}`)
      .authToken(randomUser.accessToken)
      .data();

    expect(res.isSuccess).toEqual(false);

    const after = await hyperTest
      .get<UserOutput>(`/api/users/${anotherUser.info.id}`)
      .authToken(global.adminToken)
      .query();

    expect(after.isSuccess).toEqual(true);
    expect(after.user.id).toEqual(anotherUser.info.id);
  });

  it('should success to delete user (self)', async () => {
    const before = await hyperTest
      .get<UserOutput>(`/api/users/${randomUser.info.id}`)
      .authToken(global.adminToken)
      .query();

    expect(before.isSuccess).toEqual(true);
    expect(before.user.id).toEqual(randomUser.info.id);

    const res = await hyperTest
      .delete(`/api/users/${randomUser.info.id}`)
      .authToken(randomUser.accessToken)
      .data();

    expect(res.isSuccess).toEqual(true);

    const after = await hyperTest
      .get<BaseErrorOutput>(`/api/users/${randomUser.info.id}`)
      .authToken(global.adminToken)
      .query();

    expect(after.isSuccess).toEqual(false);
  });

  it('user"s post is deleted with user', async () => {
    const result = await hyperTest
      .get<BaseErrorOutput>(`/api/posts/${userPost.id}`)
      .authToken()
      .query();

    expect(result.isSuccess).toEqual(false);
  });
};

export default tests;
