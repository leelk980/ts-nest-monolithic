import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { plainToClass, Transform, Type } from 'class-transformer';
import {
  ArrayMinSize,
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

export class BaseQueryDto {
  @ApiPropertyOptional({ example: '[{ "property": "post.postLikes", "alias": "postLike" }]' })
  @IsOptional()
  @IsArray()
  @ArrayMinSize(1)
  @ValidateNested({ each: true })
  @Transform(({ value }) => {
    if (!value) return undefined;
    return plainToClass(JoinOption, JSON.parse(value));
  }) // string에서 plainObj(Array)로 변환 + instance로 변환
  joins?: JoinOption[];

  @ApiPropertyOptional({ example: '[{ "column": "title", "operator": "=", "value": "post" }]' })
  @IsOptional()
  @IsArray()
  @ArrayMinSize(1)
  @ValidateNested({ each: true })
  @Transform(({ value }) => {
    if (!value) return undefined;
    return plainToClass(WhereOption, JSON.parse(value));
  }) // string에서 plainObj(Array)로 변환 + instance로 변환
  wheres?: WhereOption[];

  @ApiPropertyOptional({ example: '[{ "column": "id", "sort": "ASC" }]' })
  @IsOptional()
  @IsArray()
  @ArrayMinSize(1)
  @ValidateNested({ each: true })
  @Transform(({ value }) => {
    if (!value) return undefined;
    return plainToClass(OrderOption, JSON.parse(value));
  }) // string에서 plainObj(Array)로 변환 + instance로 변환
  orders?: OrderOption[];

  @ApiPropertyOptional({ example: 20 })
  @IsNumber()
  @IsOptional()
  @Type(() => Number) // queryString이라 형변환이 필요함
  limit?: number;

  @ApiPropertyOptional({ example: 2 })
  @IsNumber()
  @IsOptional()
  @Type(() => Number) // queryString이라 형변환이 필요함
  page?: number;
}

class JoinOption {
  @ApiProperty()
  @IsString()
  property: string;

  @ApiProperty()
  @IsString()
  alias: string;
}

class WhereOption {
  @ApiProperty()
  @IsString()
  column: string;

  @ApiProperty()
  @IsString()
  operator: 'ilike' | '=' | 'in';

  @ApiProperty()
  @IsNotEmpty()
  value: any;
}

class OrderOption {
  @ApiProperty()
  @IsString()
  column: string;

  @ApiProperty()
  @IsString()
  sort: 'ASC' | 'DESC';
}
