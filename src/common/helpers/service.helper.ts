import { Saga } from './saga/saga';
import { SagaBuilder } from './saga/saga.builder';

export abstract class ServiceHelper {
  private cachedSaga: Record<string, Saga> = {};

  protected createSaga(sagaName: string) {
    if (this.cachedSaga[sagaName]) {
      return this.cachedSaga[sagaName];
    } else {
      return new SagaBuilder(sagaName);
    }
  }

  protected async executeSaga(saga: Saga, firstParams: Record<string, any>) {
    if (!this.cachedSaga[saga.title]) {
      this.cachedSaga[saga.title] = saga;
    }
    return saga.execute(firstParams);
  }
}
