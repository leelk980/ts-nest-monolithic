import { PostOutput } from 'src/routes/posts/postAgg';
import { createRandomPost, createRandomUser, RandomUser } from 'test/@utils';
import { HyperTest, initHyperTest } from 'test/hyperTest';

const tests = () => {
  let randomUser: RandomUser;
  let hyperTest: HyperTest;
  beforeAll(async () => {
    randomUser = await createRandomUser();
    hyperTest = await initHyperTest();
  });

  it('should fail to find post (not exist post)', async () => {
    const res = await hyperTest
      .get<PostOutput>('/api/posts/999')
      .authToken(randomUser.accessToken)
      .query();

    expect(res.isSuccess).toEqual(false);
  });

  it('should success to find post by postId', async () => {
    const targetPost = await createRandomPost(randomUser.accessToken);

    const res = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(res.post.id).toEqual(targetPost.id);
  });
};

export default tests;
