import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PostLike } from 'src/entities';
import { CommandManager } from 'src/entities';
import { Repository } from 'typeorm';
import { CreatePostLikeDto, UpdatePostLikeDto } from './dtos/postLike.dto';

@Injectable()
export class PostLikeService {
  private postLikeCommand: CommandManager<PostLike>;

  public constructor(
    @InjectRepository(PostLike)
    private readonly postLikeRepository: Repository<PostLike>,
  ) {
    this.postLikeCommand = CommandManager(this.postLikeRepository);
  }

  async createPostLike(userId: number, postId: number, data: CreatePostLikeDto) {
    const postLike = await this.postLikeCommand.createOne({ userId, postId, ...data });
    return postLike;
  }

  async updatePostLike(postLikeId: number, data: UpdatePostLikeDto) {
    await this.postLikeCommand.updateOne(postLikeId, data);
  }

  async deletePostLike(postLikeId: number) {
    await this.postLikeCommand.hardDeleteOne(postLikeId);
  }
}
/**
 * postLikeService는 command, query를 분리하지 않은 이유
 * - command는 트랜잭션이 필요한 경우에 사용할 생각. (postLikeService는 Entity가 아직 1개임)
 * - query는 post(root entity) 타고 오니까 필요없을것 같음
 */
