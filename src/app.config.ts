import { CacheModuleOptions, ValidationPipeOptions } from '@nestjs/common';
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
import { DocumentBuilder } from '@nestjs/swagger';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import * as redisStore from 'cache-manager-redis-store';
import * as dotenv from 'dotenv';

const envFilePath =
  process.env.NODE_ENV === 'production'
    ? '.env.production'
    : process.env.NODE_ENV === 'development'
    ? '.env.development'
    : '.env.test';

dotenv.config({ path: envFilePath });

export const Port = +process.env.PORT || 4000;

export const TypeOrmConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: 5432,
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  schema: process.env.DB_SCHEMA,
  entities: [__dirname + '/**/*.entity{.ts,.js}'],
  synchronize: process.env.DB_SYNC === 'true',
  logging: false,
  keepConnectionAlive: true,
};

export const JwtConfig = {
  secret: process.env.JWT_SECRET,
  accessExpiresIn: process.env.JWT_ACCESS_EXPIRES_IN,
  refreshExpiresIn: process.env.JWT_REFRESH_EXPIRES_IN,
};

export const RedisConfig: CacheModuleOptions & { host: string; port: number } = {
  ttl: 10,
  store: redisStore,
  host: process.env.REDIS_HOST,
  port: +process.env.REIDS_PORT,
};

export const ValidationPipeConfig: ValidationPipeOptions = {
  whitelist: true,
  forbidNonWhitelisted: true,
  transform: true,
  transformOptions: { groups: ['body'] },
  /**
   * whitelist
   * - validation을 위한 decorator가 붙어있지 않은 속성들은 제거
   *
   * forbidNonWhitelisted
   * - whitelist 설정을 켜서 걸러질 속성이 있다면 아예 Error로 처리
   *
   * tarnsform
   * - 요청에서 넘어온 자료들의 형변환
   *
   * transformOptions
   * - Serialize의 Expose 옵션에서 groups가 설정되면,
   * - req.body를 DTO class로 transform 할때 groups를 보고 필터링함
   *   (transformOptions 옵션이 없으면 groups 붙은거는 다 누락됨)
   * - groups에 body가 있으면 필터링(누락) 되지 않도록함
   *   => Entity의 Expose 데코레이터의 groups에 'body'까지 명시해야함 ex) user.role
   *
   * => 정리: Expose를 붙인 property를 req.body로도 받으려면 groups에 body까지 넣어줘야함
   **/
};

export const CorsOption: CorsOptions = {
  origin: ['http://localhost:5500'],
  methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
  preflightContinue: false,
  credentials: true,
};

export const RateLimitOption = {
  max: 100,
  windowMs: 60 * 1000,
  message: 'Too many requests from this IP',
};

export const HppOption = { whitelist: ['one', 'two', 'three'] };

export const SwaggerConfig = new DocumentBuilder()
  .setTitle('API Documents')
  .setDescription('The API description')
  .addTag('API Version 1.0')
  .setVersion('1.0')
  .addBearerAuth()
  .build();
