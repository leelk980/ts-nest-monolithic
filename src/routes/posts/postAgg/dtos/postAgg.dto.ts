import { ApiProperty, PartialType, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { ArrayMinSize, IsArray, ValidateNested } from 'class-validator';
import { Post, PostCategory } from 'src/entities';

export class CreatePostAggDto extends PickType(Post, ['title', 'content']) {
  @ApiProperty({ type: () => [PickType(PostCategory, ['categoryId', 'relevance'])] })
  @IsArray()
  @ArrayMinSize(1)
  @Type(() => PickType(PostCategory, ['categoryId', 'relevance']))
  @ValidateNested({ each: true })
  postCategories: Pick<PostCategory, 'categoryId' | 'relevance'>[];
}

export class UpdatePostAggDto extends PartialType(CreatePostAggDto) {}

/**
 * CreatePostInputDto에서 postCategories를 PickType으로 가져오지 않는 이유
 * - swagger가 중첩되는 걸 막기위해 entity에서는 join property의 example을 string으로 처리하고 있음.
 *   하지만 위와 같이 join property를 input으로 받는 경우에는 swagger에서 해당 input의 example을 원래 상태대로 표현하고 싶음.
 * => 정리하면 input, output에서 example을 다르게 표현하고 싶기 때문에 PickType으로 가져오지 않음.
 *
 * - 추가로 join property는 객체라 상황에 따라 Pick하는게 달라질 수도 있음
 */
