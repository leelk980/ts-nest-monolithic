import { EventEmitter2 } from '@nestjs/event-emitter';

export abstract class ExternalHelper {
  protected readonly eventEmitter: EventEmitter2 & {
    send?: <T>(event: string, ...values: any[]) => Promise<T>;
  };

  public constructor(eventEmitter: EventEmitter2) {
    this.eventEmitter = eventEmitter;
    this.eventEmitter.send = async <T>(event: string, ...values: any[]) => {
      const res = await this.eventEmitter.emitAsync(event, ...values);
      return res[0] as T;
    };
  }
}
