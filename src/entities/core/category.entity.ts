import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { BaseEntity } from 'src/common';
import { Column, Entity, OneToMany, Tree, TreeChildren, TreeParent } from 'typeorm';
import { PostCategory, UserCategory } from '../index';

export enum CategoryNameEnum {
  ROOT1 = 'root1',
  ROOT2 = 'root2',

  ROOT1_SUB1 = 'root1_sub1',
  ROOT1_SUB2 = 'root1_sub2',
  ROOT1_SUB3 = 'root1_sub3',
  ROOT2_SUB1 = 'root2_sub1',
  ROOT2_SUB2 = 'root2_sub2',
  ROOT2_SUB3 = 'root2_sub3',

  /** LAST_DEPTH_ID = 9~20 + a */
  ROOT1_SUB1_SUB1 = 'root1_sub1_sub1',
  ROOT1_SUB1_SUB2 = 'root1_sub1_sub2',
  ROOT1_SUB2_SUB1 = 'root1_sub2_sub1',
  ROOT1_SUB2_SUB2 = 'root1_sub2_sub2',
  ROOT1_SUB3_SUB1 = 'root1_sub3_sub1',
  ROOT1_SUB3_SUB2 = 'root1_sub3_sub2',
  ROOT2_SUB1_SUB1 = 'root2_sub1_sub1',
  ROOT2_SUB1_SUB2 = 'root2_sub1_sub2',
  ROOT2_SUB2_SUB1 = 'root2_sub2_sub1',
  ROOT2_SUB2_SUB2 = 'root2_sub2_sub2',
  ROOT2_SUB3_SUB1 = 'root2_sub3_sub1',
  ROOT2_SUB3_SUB2 = 'root2_sub3_sub2',
}

/** 이거 맞나... */
const lastDepthStart = 9;
const lastDepthEnd = 20;
const addtionalLastDepth = [];

export const lastDepthCategoryIds = Array.from(
  { length: lastDepthEnd - lastDepthStart + 1 },
  (_, i) => i + lastDepthStart,
).concat(addtionalLastDepth);

@Entity()
@Tree('materialized-path')
export class Category extends BaseEntity {
  /** Table Columns */
  @ApiProperty({ example: 'category name' })
  @Column({ type: 'enum', enum: CategoryNameEnum, unique: true })
  @IsString()
  name: CategoryNameEnum;

  @ApiProperty({ example: 'category description' })
  @Column({ type: 'varchar' })
  @IsString()
  description: string;

  /** Relations */
  @ApiPropertyOptional({ example: 'Parent Category', type: () => Category }) // *lazy-loading
  @TreeParent()
  parent: Category;

  @ApiPropertyOptional({ example: ['ChildCategory1', 'ChildCategory2'], type: () => [Category] }) // *lazy-loading
  @TreeChildren()
  children: Category[];

  @ApiPropertyOptional({ example: ['UserCategory1', 'UserCategory2'], type: () => [UserCategory] }) // *lazy-loading
  @OneToMany(() => UserCategory, (userCategory) => userCategory.category)
  userCategories?: UserCategory[];

  @ApiPropertyOptional({ example: ['PostCategory1', 'PostCategory2'], type: () => [PostCategory] }) // *lazy-loading
  @OneToMany(() => PostCategory, (postCategory) => postCategory.category)
  postCategories?: PostCategory[];
}

/**
 * Should insert seed data
 */
