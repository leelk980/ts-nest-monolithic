import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsBoolean, IsNumber } from 'class-validator';
import { BaseEntity } from 'src/common';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Post, User } from '../index';

@Entity()
@Index(['userId', 'postId'], { unique: true })
export class PostLike extends BaseEntity {
  /** Table Columns */
  @ApiProperty({ example: true })
  @Column({ type: 'boolean' })
  @IsBoolean()
  isLiked: boolean;

  /** Foreign keys */
  @ApiProperty({ example: 100 })
  @Column({ type: 'int' })
  @IsNumber()
  userId: number;

  @ApiProperty({ example: 100 })
  @Column({ type: 'int' })
  @IsNumber()
  postId: number;

  /** Relations */
  @ApiPropertyOptional({ example: 'A user who like/dislike target post', type: () => User }) // *lazy-loading
  @ManyToOne(() => User, (user) => user.postLikes)
  @JoinColumn()
  user?: User;

  @ApiPropertyOptional({ example: 'Target post', type: () => Post }) // *lazy-loading
  @ManyToOne(() => Post, (post) => post.postLikes)
  @JoinColumn()
  post?: Post;
}
