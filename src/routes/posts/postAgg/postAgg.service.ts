import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { BaseQueryDto, ServiceHelper } from 'src/common';
import { PostAggCommand } from './domains/postAgg.command';
import { PostAggQuery } from './domains/postAgg.query';
import { CreatePostAggDto, UpdatePostAggDto } from './dtos/postAgg.dto';

@Injectable()
export class PostAggService extends ServiceHelper {
  public constructor(
    private readonly postAggCommand: PostAggCommand,
    private readonly postAggQuery: PostAggQuery,
  ) {
    super();
  }

  /** Commands */
  @OnEvent('post.create')
  async createPostAgg(userId: number, data: CreatePostAggDto) {
    const post = await this.postAggCommand.createPostAgg(userId, data);
    return post;
  }

  @OnEvent('post.update')
  async updatePostAgg(postId: number, data: UpdatePostAggDto) {
    await this.postAggCommand.updatePostAgg(postId, data);
  }

  @OnEvent('post.delete')
  async softDeletePostAgg(postId: number) {
    await this.postAggCommand.softDeletePostAgg(postId);
  }

  /** Queries */
  @OnEvent('post.findOne')
  async findOnePostById(postId: number) {
    const post = await this.postAggQuery.findOnePostById(postId);
    return post;
  }

  @OnEvent('post.findAll')
  async findAllPostsByQuery(query: BaseQueryDto) {
    const posts = await this.postAggQuery.findAllPostsByQuery(query);
    return posts;
  }
}
