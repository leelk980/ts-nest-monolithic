import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { BaseQueryDto } from 'src/common';
import { ExternalHelper } from 'src/common/helpers/external.helper';
import { User } from 'src/entities';

@Injectable()
export class UserExternal extends ExternalHelper {
  public constructor(eventEmitter: EventEmitter2) {
    super(eventEmitter);
  }

  /** Commands */
  async createUserAgg(data: Record<string, any>) {
    const res = await this.eventEmitter.send<User>('user.create', data);
    return res;
  }

  async updateUserAgg(userId: number, data: Record<string, any>) {
    await this.eventEmitter.send('user.update', userId, data);
  }

  async softDeleteUserAgg(userId: number) {
    await this.eventEmitter.send('user.delete', userId);
  }

  /** Queries */
  async findOneUserById(userId: number) {
    const user = await this.eventEmitter.send<User>('user.findOne', userId);
    return user;
  }

  async findAllUserByQuery(query: BaseQueryDto) {
    const users = await this.eventEmitter.send<User[]>('user.findAll', query);
    return users;
  }
}
