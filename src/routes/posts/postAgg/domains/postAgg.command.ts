import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CommandHelper } from 'src/common';
import { CommandManager, Post, PostCategory } from 'src/entities';
import { TrxService } from 'src/globals';
import { QueryRunner, Repository } from 'typeorm';
import { CreatePostAggDto, UpdatePostAggDto } from '../dtos/postAgg.dto';

@Injectable()
export class PostAggCommand extends CommandHelper {
  private postCommand: CommandManager<Post>;
  private postCategoryCommand: CommandManager<PostCategory>;

  public constructor(
    @InjectRepository(Post)
    private readonly postRepository: Repository<Post>,
    @InjectRepository(PostCategory)
    private readonly postCategoryRepository: Repository<PostCategory>,
    protected readonly trxService: TrxService,
  ) {
    super();
  }

  protected registerCommandManagers(queryRunner?: QueryRunner) {
    commandInit.call(this, queryRunner);
  }

  async createPostAgg(userId: number, data: CreatePostAggDto) {
    return await this.withTransaction(async () => {
      const { title, content, postCategories } = data;

      const post = await this.postCommand.createOne({ userId, title, content });
      await this.postCategoryCommand.createMany(
        postCategories.map(({ categoryId, relevance }) => ({
          postId: post.id,
          categoryId,
          relevance,
        })),
      );

      return post;
    });
  }

  async updatePostAgg(postId: number, data: UpdatePostAggDto) {
    await this.withTransaction(async () => {
      const { title, content, postCategories } = data;

      await this.postCommand.updateOne(postId, { title, content });

      if (Array.isArray(postCategories)) {
        await this.postCategoryCommand.hardDeleteMany({ postId });
        await this.postCategoryCommand.createMany(
          postCategories.map(({ categoryId, relevance }) => ({ postId, categoryId, relevance })),
        );
      }
    });
  }

  async softDeletePostAgg(postId: number) {
    await this.withTransaction(async () => {
      await this.postCommand.softDeleteOne(postId);
    });
  }
}

async function commandInit(queryRunner?: QueryRunner) {
  if (queryRunner) {
    this.postCommand = CommandManager(queryRunner.manager.getRepository(Post));
    this.postCategoryCommand = CommandManager(queryRunner.manager.getRepository(PostCategory));
  } else {
    this.postCommand = CommandManager(this.postRepository);
    this.postCategoryCommand = CommandManager(this.postCategoryRepository);
  }
}
