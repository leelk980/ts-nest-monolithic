import { Body, Controller, Delete, Param, Post, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ApiDefinition, CurrUser, EvictCache } from 'src/common';
import { CreatePostLikeDto, UpdatePostLikeDto } from './dtos/postLike.dto';
import { PostLikeOutput } from './dtos/postLike.output';
import { PostLikeService } from './postLike.service';

@ApiTags('PostLike')
@Controller('/posts')
@EvictCache('/likes')
export class PostLikeController {
  constructor(private readonly postLikeService: PostLikeService) {}

  @Post('/:postId/likes')
  @ApiDefinition({
    summary: '게시글에 좋아요/싫어요하기',
    response: PostLikeOutput,
  })
  async createPostLike(
    @CurrUser('id') userId: number,
    @Param('postId') postId: number,
    @Body() data: CreatePostLikeDto,
  ): Promise<PostLikeOutput> {
    const postLike = await this.postLikeService.createPostLike(userId, postId, data);
    return { postLike };
  }

  @Put('/:postId/likes/:id')
  @ApiDefinition({
    summary: '게시글 좋아요/싫어요 수정하기',
    owner: 'post_like',
  })
  async updatePostLike(@Param('id') postLikeId: number, @Body() data: UpdatePostLikeDto) {
    await this.postLikeService.updatePostLike(postLikeId, data);
  }

  @Delete('/:postId/likes/:id')
  @ApiDefinition({
    summary: '게시글 좋아요/싫어요 삭제하기',
    owner: 'post_like',
  })
  async deletePostLike(@Param('id') postLikeId: number) {
    await this.postLikeService.deletePostLike(postLikeId);
  }
}
