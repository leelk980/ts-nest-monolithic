import { BaseErrorOutput } from 'src/common';
import { Post } from 'src/entities';
import { PostOutput } from 'src/routes/posts/postAgg';
import { CreatePostLikeDto, PostLikeOutput } from 'src/routes/posts/postLike';
import { createRandomPost, createRandomUser, RandomUser } from 'test/@utils';
import { HyperTest, initHyperTest } from 'test/hyperTest';

const tests = () => {
  let randomUser: RandomUser;
  let targetPost: Post;
  let hyperTest: HyperTest;
  beforeAll(async () => {
    randomUser = await createRandomUser();
    targetPost = await createRandomPost(randomUser.accessToken);
    hyperTest = await initHyperTest();
  });

  const data: CreatePostLikeDto = { isLiked: true };

  it('should fail to create postLikes (not exist post)', async () => {
    const res = await hyperTest
      .post<BaseErrorOutput>('/api/posts/999/likes')
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });

  it('should fail to create postLikes (invalid data)', async () => {
    const res = await hyperTest
      .post<BaseErrorOutput>(`/api/posts/${targetPost.id}/likes`)
      .authToken(randomUser.accessToken)
      .data({ isLiked: '123' });

    expect(res.isSuccess).toEqual(false);
  });

  it('should success to create postLikes', async () => {
    const before = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(before.post.postLikes.length).toEqual(0);

    const res = await hyperTest
      .post<PostLikeOutput>(`/api/posts/${targetPost.id}/likes`)
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(true);

    const after = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(after.post.postLikes.length).toEqual(1);
  });
};

export default tests;
