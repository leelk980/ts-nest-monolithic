import { Body, Controller, Delete, Param, Post, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ApiDefinition, CurrUser, EvictCache } from 'src/common';
import { CreatePostCommentDto, UpdatePostCommentDto } from './dtos/postComment.dto';
import { PostCommentOutput } from './dtos/postComment.output';
import { PostCommentService } from './index';

@ApiTags('PostComment')
@Controller('/posts')
@EvictCache('/comments')
export class PostCommentController {
  public constructor(private readonly postCommentService: PostCommentService) {}

  @Post('/:postId/comments')
  @ApiDefinition({
    summary: '게시글에 댓글 작성하기',
    response: PostCommentOutput,
  })
  async createPostComment(
    @CurrUser('id') userId: number,
    @Param('postId') postId: number,
    @Body() data: CreatePostCommentDto,
  ): Promise<PostCommentOutput> {
    const postComment = await this.postCommentService.createPostComment(userId, postId, data);
    return { postComment };
  }

  @Put('/:postId/comments/:id')
  @ApiDefinition({
    summary: '게시글의 댓글 수정하기',
    owner: 'post_comment',
  })
  async updatePostComment(@Param('id') postCommentId: number, @Body() data: UpdatePostCommentDto) {
    await this.postCommentService.updatePostComment(postCommentId, data);
  }

  @Delete('/:postId/comments/:id')
  @ApiDefinition({
    summary: '게시글의 댓글 삭제하기',
    owner: 'post_comment',
  })
  async softDeletePostComment(@Param('id') postCommentId: number) {
    await this.postCommentService.softDeletePostComment(postCommentId);
  }
}
