import { ApiProperty, PickType } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { ArrayMinSize, IsArray, ValidateNested } from 'class-validator';
import { User, UserCategory } from 'src/entities';

export class CreateUserAggDto extends PickType(User, ['name', 'email', 'password', 'role']) {
  @ApiProperty({ type: () => [PickType(UserCategory, ['categoryId', 'preference'])] })
  @IsArray()
  @ArrayMinSize(1)
  @Type(() => PickType(UserCategory, ['categoryId', 'preference']))
  @ValidateNested({ each: true })
  userCategories: Pick<UserCategory, 'categoryId' | 'preference'>[];
}

export class UpdateUserAggDto extends PickType(User, ['name', 'role']) {}
