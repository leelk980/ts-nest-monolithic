export * from './domains/postAgg.command';
export * from './domains/postAgg.query';
export * from './postAgg.service';
export * from './postAgg.controller';

export * from './dtos/postAgg.dto';
export * from './dtos/postAgg.output';
