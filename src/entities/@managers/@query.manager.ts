import { BaseEntity, BaseQueryDto } from 'src/common';
import { Repository } from 'typeorm';

class Entity extends BaseEntity {
  [prop: string]: any;
}

export type QueryManager<T extends Entity> = {
  findAllByQuery: (query?: BaseQueryDto) => Promise<T[]>;
};

export function QueryManager<T extends Entity>(entityRepository: Repository<T>): QueryManager<T> {
  const entityName = entityRepository.metadata.name.toLocaleLowerCase();

  async function findAllByQuery(query: BaseQueryDto) {
    const qb = entityRepository.createQueryBuilder(entityName);
    const { joins, wheres, orders, limit, page } = query;

    if (joins) {
      for (const joinOption of joins) {
        const { property, alias } = joinOption;
        qb.leftJoinAndSelect(property, alias);
      }
    }

    if (wheres) {
      for (const whereOption of wheres) {
        const { column, operator, value } = whereOption;
        qb.andWhere(`${entityName}.${column} ${operator} :value`, { value });
      }
    }

    if (orders) {
      for (const orderOption of orders) {
        const { column, sort } = orderOption;
        qb.addOrderBy(`${entityName}.${column}`, sort);
      }
    }

    const take = limit ? limit : 100;
    const skip = page ? (page - 1) * take : 0;

    qb.skip(skip).take(take);

    return await qb.getMany();
  }

  return {
    findAllByQuery,
  };
}

/**
 * Manager:
 * Repository를 Argument로 넣으면,
 * Repository를 조회하는 함수 리턴
 * queryBuilder에 dependency를 걸어서 사용할 예정
 */
