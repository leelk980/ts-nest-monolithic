import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Post, PostCategory, PostComment, PostLike } from 'src/entities';
import { PostAggCommand, PostAggController, PostAggQuery, PostAggService } from './postAgg';
import { PostCommentController, PostCommentService } from './postComment';
import { PostLikeController, PostLikeService } from './postLike';
import ExternalModule from '../@externals';

// internal domain
const PostServices = [PostAggService, PostLikeService, PostCommentService];
const PostDomains = [PostAggCommand, PostAggQuery];

@Module({
  imports: [TypeOrmModule.forFeature([Post, PostCategory, PostLike, PostComment]), ExternalModule],
  controllers: [PostAggController, PostLikeController, PostCommentController],
  providers: [...PostServices, ...PostDomains],
  exports: [...PostServices],
})
export class PostModule {}
