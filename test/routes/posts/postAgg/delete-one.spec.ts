import { createRandomPost, createRandomUser, RandomUser } from 'test/@utils';
import { HyperTest, initHyperTest } from 'test/hyperTest';

const tests = () => {
  let randomUser: RandomUser;
  let hyperTest: HyperTest;
  beforeAll(async () => {
    randomUser = await createRandomUser();
    hyperTest = await initHyperTest();
  });

  it('should fail to delete post (not exist post)', async () => {
    const res = await hyperTest
      .delete('/api/posts/999')
      .authToken(randomUser.accessToken)
      .data(emptyObj);

    expect(res.isSuccess).toEqual(false);
  });

  it('should success to delete post', async () => {
    const { id } = await createRandomPost(randomUser.accessToken);

    const res = await hyperTest
      .delete(`/api/posts/${id}`)
      .authToken(randomUser.accessToken)
      .data(emptyObj);

    expect(res.isSuccess).toEqual(true);

    const confirm = await hyperTest
      .get(`/api/posts/${id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(confirm.isSuccess).toEqual(false);
  });

  it('should fail to delete post (forbidden resource)', async () => {
    const anotherUser = await createRandomUser();
    const anotherUsersPost = await createRandomPost(anotherUser.accessToken);

    const res = await hyperTest
      .delete(`/api/posts/${anotherUsersPost.id}`)
      .authToken(randomUser.accessToken)
      .data(emptyObj);

    expect(res.isSuccess).toEqual(false);

    const confirm = await hyperTest
      .get(`/api/posts/${anotherUsersPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(confirm.isSuccess).toEqual(true);
  });
};

export default tests;
