import createOneTest from './create-one.spec';
import deleteOneTest from './delete-one.spec';
import findAllTest from './find-all.spec';
import findOneTest from './find-one.spec';
import updateOneTest from './update-one.spec';

const PostAggTest = () => {
  describe('[POST] /api/posts => createOneTest', createOneTest);
  describe('[GET] /api/posts/:id => findOneTest', findOneTest);
  describe('[GET] /api/posts => findAllTest', findAllTest);
  describe('[PUT] /api/posts/:id => updateOneTest', updateOneTest);
  describe('[DELETE] /api/posts/:id => deleteOneTest', deleteOneTest);
};

export default PostAggTest;
