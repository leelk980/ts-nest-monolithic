import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User, UserCategory } from 'src/entities';
import { UserAggCommand, UserAggController, UserAggQuery, UserAggService } from './userAgg';
import ExternalModule from '../@externals';

const UserServices = [UserAggService];
const UserDomains = [UserAggCommand, UserAggQuery];

@Module({
  imports: [TypeOrmModule.forFeature([User, UserCategory]), ExternalModule],
  controllers: [UserAggController],
  providers: [...UserServices, ...UserDomains],
})
export class UserModule {}
