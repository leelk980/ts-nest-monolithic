import { isTypeOf } from 'src/@utils';
import { Post, PostComment } from 'src/entities';
import { PostOutput } from 'src/routes/posts/postAgg';
import { CreatePostCommentDto, PostCommentOutput } from 'src/routes/posts/postComment';
import { createRandomPost, createRandomUser, RandomUser } from 'test/@utils';
import { HyperTest, initHyperTest } from 'test/hyperTest';

const tests = () => {
  let randomUser: RandomUser;
  let targetPost: Post;
  let targetPostComment: PostComment;
  let hyperTest: HyperTest;
  beforeAll(async () => {
    randomUser = await createRandomUser();
    targetPost = await createRandomPost(randomUser.accessToken);
    hyperTest = await initHyperTest();

    const res = await hyperTest
      .post<PostCommentOutput>(`/api/posts/${targetPost.id}/comments`)
      .authToken(randomUser.accessToken)
      .data(isTypeOf<CreatePostCommentDto>({ content: 'content' }));

    targetPostComment = res.postComment;
  });

  it('should success to delete comment', async () => {
    const before = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(before.post.postComments.length).toEqual(1);

    const res = await hyperTest
      .delete(`/api/posts/${targetPost.id}/comments/${targetPostComment.id}`)
      .authToken(randomUser.accessToken)
      .data();

    expect(res.isSuccess).toEqual(true);

    const after = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(after.post.postComments.length).toEqual(0);
  });

  it('should fail to delete other`s comment', async () => {
    const anotherUser = await createRandomUser();
    const { postComment: anotherUserComment } = await hyperTest
      .post<PostCommentOutput>(`/api/posts/${targetPost.id}/comments`)
      .authToken(anotherUser.accessToken)
      .data(isTypeOf<CreatePostCommentDto>({ content: 'content' }));

    const before = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(before.post.postComments.length).toEqual(1);

    const res = await hyperTest
      .delete(`/api/posts/${targetPost.id}/comments/${anotherUserComment.id}`)
      .authToken(randomUser.accessToken)
      .data();

    expect(res.isSuccess).toEqual(false);

    const after = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(after.post.postComments.length).toEqual(1);
  });
};

export default tests;
