import { PickType } from '@nestjs/swagger';
import { PostComment } from 'src/entities';

export class CreatePostCommentDto extends PickType(PostComment, ['content', 'parentId']) {}

export class UpdatePostCommentDto extends PickType(PostComment, ['content']) {}
