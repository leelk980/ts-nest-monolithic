import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseQueryDto } from 'src/common';
import { User } from 'src/entities';
import { UserQueryManager } from 'src/entities';
import { Repository } from 'typeorm';

@Injectable()
export class UserAggQuery {
  private readonly userQuery: UserQueryManager;

  public constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {
    this.userQuery = UserQueryManager(this.userRepository);
  }

  async findOneByUserId(userId: number) {
    const user = await this.userQuery
      .build()
      .id(userId)
      .leftJoinAndSelect('user.posts', 'post')
      .leftJoinAndSelect('user.postLikes', 'postLikes')
      .getOneOrFail();

    return user;
  }

  async findAllUsersByQuery(query: BaseQueryDto) {
    if (!query.joins) {
      query.joins = [{ property: 'user.posts', alias: 'post' }];
    }
    const users = await this.userQuery.manager.findAllByQuery(query);
    return users;
  }
}
