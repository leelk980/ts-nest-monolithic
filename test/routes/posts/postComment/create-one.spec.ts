import { BaseErrorOutput } from 'src/common';
import { Post } from 'src/entities';
import { PostOutput } from 'src/routes/posts/postAgg';
import { CreatePostCommentDto, PostCommentOutput } from 'src/routes/posts/postComment';
import { createRandomPost, createRandomUser, RandomUser } from 'test/@utils';
import { HyperTest, initHyperTest } from 'test/hyperTest';

const tests = () => {
  let randomUser: RandomUser;
  let targetPost: Post;
  let hyperTest: HyperTest;
  beforeAll(async () => {
    randomUser = await createRandomUser();
    targetPost = await createRandomPost(randomUser.accessToken);
    hyperTest = await initHyperTest();
  });

  const data: CreatePostCommentDto = {
    content: 'comments',
  };

  it('should fail to create comment (not exist post)', async () => {
    const res = await hyperTest
      .post<BaseErrorOutput>(`/api/posts/999/comments/`)
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });

  it('should success to create post comment', async () => {
    const before = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(before.post.postComments.length).toEqual(0);

    const res = await hyperTest
      .post<PostCommentOutput>(`/api/posts/${targetPost.id}/comments`)
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(true);

    const after = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(after.post.postComments.length).toEqual(1);
  });

  it('should success to create post subComment', async () => {
    const before = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(before.post.postComments.length).toEqual(1);
    expect(before.post.postComments[0].children.length).toEqual(0);

    const parent = before.post.postComments[0];
    const res = await hyperTest
      .post<PostCommentOutput>(`/api/posts/${targetPost.id}/comments`)
      .authToken(randomUser.accessToken)
      .data({ ...data, parentId: parent.id });

    expect(res.isSuccess).toEqual(true);

    const after = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(after.post.postComments.length).toEqual(1);
    expect(after.post.postComments[0].children.length).toEqual(1);
  });
};

export default tests;
