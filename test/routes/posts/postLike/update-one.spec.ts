import { BaseErrorOutput } from 'src/common';
import { Post, PostLike } from 'src/entities';
import { PostOutput } from 'src/routes/posts/postAgg';
import { PostLikeOutput, UpdatePostLikeDto } from 'src/routes/posts/postLike';
import { createRandomPost, createRandomUser, RandomUser } from 'test/@utils';
import { HyperTest, initHyperTest } from 'test/hyperTest';

const tests = () => {
  let randomUser: RandomUser;
  let targetPost: Post;
  let targetPostLike: PostLike;
  let hyperTest: HyperTest;
  beforeAll(async () => {
    randomUser = await createRandomUser();
    targetPost = await createRandomPost(randomUser.accessToken);
    hyperTest = await initHyperTest();

    const res = await hyperTest
      .post<PostLikeOutput>(`/api/posts/${targetPost.id}/likes`)
      .authToken(randomUser.accessToken)
      .data({ isLiked: true });

    targetPostLike = res.postLike;
  });

  const data: UpdatePostLikeDto = { isLiked: false };

  it('should fail to update postLikes (not exist postLikes)', async () => {
    const res = await hyperTest
      .put<BaseErrorOutput>(`/api/posts/${targetPost.id}/likes/999`)
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });

  it('should fail to update postLikes (invalid data)', async () => {
    const res = await hyperTest
      .put(`/api/posts/${targetPost.id}/likes/${targetPostLike.id}`)
      .authToken(randomUser.accessToken)
      .data({ isLiked: '123' });

    expect(res.isSuccess).toEqual(false);
  });

  it('should success to update postLikes (as false)', async () => {
    const before = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(before.post.postLikes.length).toEqual(1);
    expect(before.post.postLikes[0].isLiked).toEqual(true);

    const res = await hyperTest
      .put(`/api/posts/${targetPost.id}/likes/${targetPostLike.id}`)
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(true);

    const after = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(after.post.postLikes.length).toEqual(1);
    expect(after.post.postLikes[0].isLiked).toEqual(false);
  });

  it('should fail to update postLikes (not owner)', async () => {
    const anotherUser = await createRandomUser();
    const { postLike: anotherUsersPostLike } = await hyperTest
      .post<PostLikeOutput>(`/api/posts/${targetPost.id}/likes`)
      .authToken(anotherUser.accessToken)
      .data({ isLiked: true });

    const res = await hyperTest
      .put<BaseErrorOutput>(`/api/posts/${targetPost.id}/likes/${anotherUsersPostLike.id}`)
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });
};

export default tests;
