import { HttpStatus, Injectable, UnauthorizedException } from '@nestjs/common';
import { generateCharacters } from 'src/@utils';
import { AppException } from 'src/common';
import { JwtService } from 'src/globals';
import { RefreshAccessTokenDto, SignInDto, SignUpDto } from './dtos/auth.dto';
import { UserExternal } from '../@externals';

@Injectable()
export class AuthService {
  public constructor(
    private readonly userExternal: UserExternal,
    private readonly jwtService: JwtService,
  ) {}

  async signUpUser(data: SignUpDto) {
    const user = await this.userExternal.createUserAgg(data);
    const authTokens = await this.jwtService.issueAuthTokens({
      id: user.id,
      secretKey: user.secretKey,
    });

    return authTokens;
  }

  async signInUser(data: SignInDto) {
    const { email, password } = data;

    const user = (
      await this.userExternal.findAllUserByQuery({
        wheres: [{ column: 'email', operator: '=', value: email }],
      })
    )[0];
    if (!user || !(await user.comparePassword(password)))
      throw new AppException('invalid email or password', HttpStatus.BAD_REQUEST);

    const secretKey = generateCharacters(9);
    await this.userExternal.updateUserAgg(user.id, { secretKey, updatedAt: user.updatedAt });

    const authTokens = await this.jwtService.issueAuthTokens({ id: user.id, secretKey });
    return authTokens;
  }

  async refreshAccessToken(data: RefreshAccessTokenDto) {
    const { accessToken, refreshToken } = data;

    const accessTokenPayload = await this.jwtService.decodeExpiredAccessToken(accessToken);
    const refreshTokenPayload = await this.jwtService.decodeRefreshToken(refreshToken);
    if (
      accessTokenPayload.id !== refreshTokenPayload.id ||
      accessTokenPayload.secretKey !== refreshTokenPayload.secretKey
    ) {
      throw new UnauthorizedException();
    }

    const user = await this.userExternal.findOneUserById(accessTokenPayload.id);
    if (!user || user.secretKey !== accessTokenPayload.secretKey) {
      throw new AppException('invalid user secretKey. please login again', HttpStatus.UNAUTHORIZED);
    }

    const authTokens = await this.jwtService.issueAuthTokens({
      id: user.id,
      secretKey: user.secretKey,
    });

    return authTokens.accessToken;
  }
  /*
  async createKakaoUser(data: {
    name: string;
    email: string;
    oauthId: string;
    accessToken: string;
    refreshToken: string;
  }) {
    const queryRunner = await this.trxService.getQueryRunner();
    const userTrxCommand = CommandManager(queryRunner.manager.getRepository(User));
    const userOauthTrxCommand = CommandManager(queryRunner.manager.getRepository(UserOauth));

    const fn = async () => {
      const { email, name, oauthId, accessToken, refreshToken } = data;

      const secretKey = generateCharacters(9);
      const user = await userTrxCommand.createOne({
        email,
        name,
        secretKey,
        role: UserRoleEnum.User,
      });
      await userOauthTrxCommand.createOne({
        oauthId,
        accessToken,
        refreshToken,
        oauthProvider: UserOauthProviderEnum.Kakao,
        userId: user.id,
      });

      return user;
    };

    const newKakaoAuthUser = await this.trxService.runTransaction(queryRunner, fn);
    const authTokens = await this.jwtService.issueAuthTokens({
      id: newKakaoAuthUser.id,
      secretKey: newKakaoAuthUser.secretKey,
    });

    return authTokens;
  }

  async loginKakaoUser(oauthId: string, refreshToken: string) {
    const user = await this.userQuery
      .build()
      .leftJoinAndSelect('user.userOauth', 'userOauth')
      .andWhere('userOauth.oauthId = :oauthId', { oauthId })
      .andWhere('userOauth.oauthProvider = :provider', { provider: UserOauthProviderEnum.Kakao })
      .getOne();

    if (!user) return false;

    if (user.userOauth.refreshToken !== refreshToken) {
      await this.userOauthCommand.updateOne(user.userOauth.id, { refreshToken });
    }

    const secretKey = generateCharacters(9);
    await this.userCommand.updateOne(user.id, { secretKey, updatedAt: user.updatedAt });
    const authTokens = await this.jwtService.issueAuthTokens({ id: user.id, secretKey });
    return authTokens;
  }
  */
}

/**
 * TODO: secretKey 바꾸고 나서 EvictCache 안해서, 10초(cache ttl) 동안 구버전 토큰이 동작함
 * TODO: 비밀번호 바꾸고 나서도 sercetKey 변경해야함
 * 이렇게 secretKey를 accessToken에 넣으면 다중기기에서 동시 로그인 안될듯
 * => valid한 refreshToken을 기기끼리 공유해야함
 */
