import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import * as bcrypt from 'bcrypt';
import { Exclude, Expose } from 'class-transformer';
import { IsEmail, IsEnum, IsString } from 'class-validator';
import { BaseEntity } from 'src/common';
import { Column, Entity, OneToMany, OneToOne } from 'typeorm';
import { Post, PostComment, PostLike, UserCategory, UserOauth } from '../index';

export enum UserRoleEnum {
  Admin = 'admin',
  User = 'user',
}

@Entity()
export class User extends BaseEntity {
  /** Table Columns */
  @ApiProperty({ example: 'test' })
  @Column({ type: 'varchar' })
  @IsString()
  name: string;

  @ApiProperty({ example: 'test1@test.com' })
  @Column({ type: 'varchar', unique: true })
  @IsEmail()
  email: string;

  /**
   * @TODO
   * - role은 admin한테만 Expose 되어야함(일반 유저한테는 비공개)
   * - 그래도 회원가입시 req.body로는 받아야 함 => *별도로 Pipe 설정 필요*
   *
   * @groups
   * - admin: "응답" 보낼때 Serialize 옵션에 admin이 있으면 Expose함
   * - body: "요청" 받을때 req.body에 role을 받으려면 groups에 'body'까지 명시해야함(Pipe 옵션)
   */
  @Expose({ groups: ['admin', 'body'] })
  // @Transform((params) => (params.value === UserRoleEnum.Admin ? '관리자' : '일반회원'))
  @ApiProperty({ example: 'user' })
  @Column({ type: 'enum', enum: UserRoleEnum })
  @IsEnum(UserRoleEnum)
  role: UserRoleEnum;

  @Exclude({ toPlainOnly: true }) // toPlainOnly: 직렬화(res 내려줄 때)에서만 제외, 역직렬화(body로 받을 때)에서는 제외되지 않음
  @ApiProperty({ example: '12341234' })
  @Column({ type: 'varchar', nullable: true })
  @IsString()
  password?: string;

  @Exclude({ toPlainOnly: true })
  @ApiProperty({ example: 'secretKey for auth token' })
  @Column({ type: 'varchar' })
  @IsString()
  secretKey: string;

  /** Relations */
  @ApiPropertyOptional({ example: 'Oauth Information', type: () => [UserOauth] }) // *lazy-loading
  @OneToOne(() => UserOauth, (userOauth) => userOauth.user)
  userOauth?: UserOauth;

  @ApiPropertyOptional({ example: ['Post1', 'Post2'], type: () => [Post] }) // *lazy-loading
  @OneToMany(() => Post, (post) => post.user)
  posts?: Post[];

  @ApiPropertyOptional({ example: ['PostComment1', 'PostComment2'], type: () => [PostComment] }) // *lazy-loading
  @OneToMany(() => PostComment, (postComment) => postComment.user)
  postComments?: PostComment[];

  @ApiPropertyOptional({ example: ['PostLike1', 'PostLike2'], type: () => [PostLike] }) // *lazy-loading
  @OneToMany(() => PostLike, (postLike) => postLike.user)
  postLikes?: PostLike[];

  @ApiPropertyOptional({ example: ['UserCategory1', 'UserCategory2'], type: () => [UserCategory] }) // *lazy-loading
  @OneToMany(() => UserCategory, (userCategory) => userCategory.user)
  userCategories?: UserCategory[];

  /** Instance Methods */
  async comparePassword(passwordInput: string) {
    return await bcrypt.compare(passwordInput, this.password);
  }
}

/**
 * Serialize: https://ed-stphnsn.medium.com/nestjs-serialization-hidden-fields-13f00e205e32
 *
 * Expose vs Exclude
 * - Expose는 선택적으로 보여주고 선택적으로 가릴 경우
 * - Exclude는 그냥 다 가릴 경우
 * (그래도 req.body는 둘 다 받을 수 있음)
 */
