import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import { BaseEntity } from 'src/common';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Post, User } from '../index';

@Entity()
export class PostComment extends BaseEntity {
  /** Table Columns */
  @ApiProperty({ example: 'contents' })
  @Column({ type: 'varchar' })
  @IsString()
  content: string;

  /** Foreign keys */
  @ApiProperty({ example: 100 })
  @Column({ type: 'int' })
  @IsNumber()
  userId: number;

  @ApiProperty({ example: 100 })
  @Column({ type: 'int' })
  @IsNumber()
  postId: number;

  @ApiPropertyOptional({ example: 100 })
  @Column({ type: 'int', nullable: true })
  @IsNumber()
  @IsOptional()
  parentId?: number;

  /** Relations */
  @ApiPropertyOptional({ example: 'A user who writes this comment', type: () => User }) // *lazy-loading
  @ManyToOne(() => User, (user) => user.postComments)
  @JoinColumn()
  user?: User;

  @ApiPropertyOptional({ example: 'Target post', type: () => Post }) // *lazy-loading
  @ManyToOne(() => Post, (post) => post.postComments)
  @JoinColumn()
  post?: Post;

  @ApiPropertyOptional({ example: 'Parent Comment', type: () => PostComment }) // *lazy-loading
  @ManyToOne(() => PostComment, (postComment) => postComment.children)
  parent?: PostComment;

  @ApiPropertyOptional({ example: 'Children Comment', type: () => [PostComment] }) // *lazy-loading
  @OneToMany(() => PostComment, (postComment) => postComment.parent)
  children?: PostComment[];
}
