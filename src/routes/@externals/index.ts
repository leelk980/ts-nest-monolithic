import { Module } from '@nestjs/common';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { PostExternal } from './post.external';
import { UserExternal } from './user.external';

const Externals = [UserExternal, PostExternal];

@Module({
  imports: [EventEmitterModule.forRoot()],
  providers: Externals,
  exports: Externals,
})
export default class ExternalModule {}

export { UserExternal, PostExternal };
