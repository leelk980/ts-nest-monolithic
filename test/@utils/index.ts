import { generateAlphaString, generateNumberString, getOneofArrayItem } from 'src/@utils';
import { lastDepthCategoryIds, User, UserRoleEnum } from 'src/entities';
import { SignUpDto } from 'src/routes/auth/dtos/auth.dto';
import { AuthTokensOutput } from 'src/routes/auth/dtos/auth.output';
import { CreatePostAggDto, PostOutput } from 'src/routes/posts/postAgg';
import { UserOutput } from 'src/routes/users/userAgg';
import { initHyperTest } from 'test/hyperTest';

// export type RandomUser = Awaited<ReturnType<typeof createRandomUser>>;
export type RandomUser = {
  accessToken: string;
  refreshToken: string;
  info: User;
};
export const createRandomUser = async () => {
  const hyperTest = await initHyperTest();

  const data: SignUpDto = {
    email: `${generateAlphaString(5)}@${generateAlphaString(5)}.com`,
    name: generateAlphaString(5),
    role: UserRoleEnum.User,
    password: '12341234',
    userCategories: [{ categoryId: getOneofArrayItem(lastDepthCategoryIds), preference: 1 }],
  };

  const { accessToken, refreshToken } = await hyperTest
    .post<AuthTokensOutput>('/api/auth/signup')
    .authToken()
    .data(data);

  const res = await hyperTest
    .get<UserOutput>('/api/users/me')
    .authToken(accessToken)
    .query(emptyObj)
    .catch(() => console.log('fail to createRandomUser'));

  if (typeof res === 'undefined') return;

  return {
    accessToken,
    refreshToken,
    info: res.user,
  };
};

export const createRandomPost = async (accessToken: string) => {
  const hyperTest = await initHyperTest();

  const data: CreatePostAggDto = {
    title: generateAlphaString(10),
    content: generateAlphaString(100),
    postCategories: [
      {
        categoryId: getOneofArrayItem(lastDepthCategoryIds),
        relevance: generateNumberString(1, 10),
      },
    ],
  };

  const res = await hyperTest
    .post<PostOutput>('/api/posts')
    .authToken(accessToken)
    .data({ ...data })
    .catch(() => console.log('fail to createRandomPost'));

  if (typeof res === 'undefined') return;

  return res.post;
};
