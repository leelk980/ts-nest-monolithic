import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { BaseQueryDto } from 'src/common';
import { ExternalHelper } from 'src/common/helpers/external.helper';

@Injectable()
export class PostExternal extends ExternalHelper {
  public constructor(eventEmitter: EventEmitter2) {
    super(eventEmitter);
  }

  async createPostAgg(userId: number, data: Record<string, any>) {
    const post = await this.eventEmitter.send('post.create', userId, data);
    return post;
  }

  async updatePostAgg(postId: number, data: Record<string, any>) {
    await this.eventEmitter.send('post.update', postId, data);
  }

  async softDeletePostAgg(postId: number) {
    await this.eventEmitter.send('post.delete', postId);
  }

  async findOnePostById(postId: number) {
    const post = await this.eventEmitter.send('post.findOne', postId);
    return post;
  }

  async findAllPostsByQuery(query: BaseQueryDto) {
    const posts = await this.eventEmitter.send('post.findAll', query);
    return posts;
  }
}
