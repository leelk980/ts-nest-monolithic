import { getOneofArrayItem } from 'src/@utils';
import { lastDepthCategoryIds, Post } from 'src/entities';
import { PostOutput, UpdatePostAggDto } from 'src/routes/posts/postAgg';
import { createRandomPost, createRandomUser, RandomUser } from 'test/@utils';
import { HyperTest, initHyperTest } from 'test/hyperTest';

const tests = () => {
  let randomUser: RandomUser;
  let targetPost: Post;
  let hyperTest: HyperTest;
  beforeAll(async () => {
    randomUser = await createRandomUser();
    targetPost = await createRandomPost(randomUser.accessToken);
    hyperTest = await initHyperTest();
  });

  const data: UpdatePostAggDto = {
    title: 'changed title',
    content: 'changed content',
    postCategories: [{ categoryId: getOneofArrayItem(lastDepthCategoryIds), relevance: 3 }],
  };

  it('should fail to update post (not exist post)', async () => {
    const res = await hyperTest
      .put('/api/posts/999')
      .authToken(randomUser.accessToken)
      .data({ ...data });

    expect(res.isSuccess).toEqual(false);
  });

  it('should fail to update post (include invalid data)', async () => {
    const res = await hyperTest
      .put(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .data({ ...data, invalid: 'invalid' });

    expect(res.isSuccess).toEqual(false);
  });

  it('should success to update title', async () => {
    const res = await hyperTest
      .put(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .data({ title: data.title });

    expect(res.isSuccess).toEqual(true);

    const confirm = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(confirm.post.title).toEqual(data.title);
  });

  it('should success to update content', async () => {
    const res = await hyperTest
      .put(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .data({ content: data.content });

    expect(res.isSuccess).toEqual(true);

    const confirm = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(confirm.post.content).toEqual(data.content);
  });

  it('should success to update postCategories', async () => {
    const res = await hyperTest
      .put(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .data({ postCategories: data.postCategories });

    expect(res.isSuccess).toEqual(true);

    const confirm = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(confirm.post.postCategories.map((item) => item.category.id)).toEqual(
      data.postCategories.map((item) => item.categoryId),
    );
  });

  it('should fail to update post (forbidden resource)', async () => {
    const anotherUser = await createRandomUser();
    const anotherUsersPost = await createRandomPost(anotherUser.accessToken);

    const res = await hyperTest
      .put(`/api/posts/${anotherUsersPost.id}`)
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });
};

export default tests;
