import { HttpStatus } from '@nestjs/common';
import { AppException } from 'src/common';
import { SagaFlow } from './saga.flow';

export enum SagaStates {
  New = 'New',
  InProgress = 'In progress',
  InCompensation = 'In compensation',
  Complete = 'Complete',
  CompensationComplete = 'Compensation complete',
  CompensationError = 'Compensation error',
}

export class Saga {
  public title: string;
  public state: SagaStates;
  private sagaFlow: SagaFlow;

  public constructor(title: string, sagaFlow: SagaFlow) {
    this.title = title;
    this.sagaFlow = sagaFlow;
    this.state = SagaStates.New;
  }

  public async execute(firstParams: Record<string, any>) {
    this.state = SagaStates.InProgress;
    try {
      const results = await this.sagaFlow.invokeSteps(firstParams);
      this.state = SagaStates.Complete;
      return results;
    } catch (err) {
      this.state = SagaStates.InCompensation;
      await this.runCompensationFlow();
      throw new AppException( // TODO: 에러처리 saga-flow에서
        JSON.stringify(`[SagaInvokeError]: ${this.title}\n ${err}`),
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  private async runCompensationFlow(): Promise<void> {
    try {
      await this.sagaFlow.compensateSteps();
      this.state = SagaStates.CompensationComplete;
    } catch (err) {
      this.state = SagaStates.CompensationError;
      throw new AppException( // TODO: 에러처리 saga-flow에서
        JSON.stringify(`[SagaCompensateError]: ${this.title}\n ${err}`),
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  /* eslint-disable */
  public setStep<Input = any, Output = any>(params: {
    name: string;
    invoke: (params: Input) => Promise<Output>;
    compensate: (params: Input) => Promise<any>;
  }) {
    return this;
  }

  public build(): Saga {
    return this;
  }
}

/**
 - Saga는 Saga-flow를 갖는다
 - Saga-flow는 Saga-step 및 results를 갖는다
 - Saga-step은 invokeFn, compensationFn을 갖는다
 
 - Saga-builder는 Saga를 만들어준다 (하위의 flow, step들 까지)
 - Step1의 invokeFn의 param은 없고 Step2부터는 이전 Step의 result가 parameter로 전달받음
 - TODO: Step2 이후 파라미터 타입 추론되게 만들기 => 이거 좀 빡센듯

 - **setStep(), build()는 SagaBuilder의 메소드인데, Saga에도 있는 이유
 - 서비스에서 this.createSaga()하면 처음에는 SagaBuilder를 리턴하고, setStep(), build()로 Saga를 만든다
 - executeSaga로 사가를 실행하면 만들어진 Saga를 클래스의 private 변수에 캐싱하고, 두번째 부터는 캐싱된 Saga를 리턴한다
 - 즉 this.createSaga()의 리턴 타입이 SagaBulder | Saga 이라, 메소드가 없어서 타입이 없는 이슈가 있어서, 껍데기만 있는  setStep(), build()를 Saga에도 추가했다.
 - 이렇게라도 캐싱을하는게 매번 요청마다 Saga를 새로 만드는것보다 성능적으로 낫겠지만, 어쨋든 껍데기 메소드를 호출하는 것은 개선해야함
 */
