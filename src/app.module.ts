import { ClassSerializerInterceptor, Module, ValidationPipe } from '@nestjs/common';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtConfig, RedisConfig, TypeOrmConfig, ValidationPipeConfig } from './app.config';
import { AllExceptionFilter, LoginGuard, ResponseInterceptor } from './common';
import * as GatewayModules from './gateways';
import * as GlobalModules from './globals';
import * as RouteModules from './routes';

@Module({
  imports: [
    TypeOrmModule.forRoot(TypeOrmConfig),
    GlobalModules.JwtModule.register(JwtConfig),
    GlobalModules.RedisModule.register(RedisConfig),
    GlobalModules.TrxModule.register(),
    GlobalModules.LoggerModule.register(),
    GlobalModules.KafkaModule.register(),
    ...Object.values(GatewayModules),
    ...Object.values(RouteModules),
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: AllExceptionFilter,
    },
    {
      provide: APP_PIPE,
      useValue: new ValidationPipe(ValidationPipeConfig),
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ResponseInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: ClassSerializerInterceptor,
    },
    {
      provide: APP_GUARD,
      useClass: LoginGuard,
    },
  ],
})
export class AppModule {}
/**
 * Interceptor 순서 매우매우 중요함
 * - ClassSerializerInterceptor가 먼저 실행되고, ResponseInterceptor가 나중에 실행되어야함
 * - why? 직렬화된 응답을 캐싱해야 하기 때문에
 */
