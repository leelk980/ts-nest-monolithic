import { SagaStep } from './saga.step';

export class SagaFlow {
  private readonly steps: SagaStep<any, any>[];
  private finishedSteps: SagaStep<any, any>[] = [];
  private results: any[];

  constructor(steps: SagaStep<any, any>[] = []) {
    this.steps = steps;
    this.results = [];
    this.finishedSteps = [];
  }

  async invokeSteps(firstParams: Record<string, any>) {
    this.finishedSteps = [];
    this.results = [];
    for (const [index, step] of this.steps.entries()) {
      this.finishedSteps.push(step);

      let result: any;
      if (index === 0) {
        result = await step.invoke(firstParams);
      } else {
        const prevStepResult = this.results[index - 1];
        result = await step.invoke(prevStepResult);
      }

      this.results.push(result);
    }

    return this.results;
  }

  async compensateSteps() {
    for (const [index, step] of this.finishedSteps.reverse().entries()) {
      if (index === this.finishedSteps.length - 1) {
        await step.compensate();
      } else {
        const params = this.results[this.finishedSteps.length - 2 - index];
        await step.compensate(params);
      }
    }
  }
}
