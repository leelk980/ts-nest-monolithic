import { ApiProperty } from '@nestjs/swagger';
import { BaseOutput } from 'src/common';
import { PostLike } from 'src/entities';

export class PostLikeOutput extends BaseOutput {
  @ApiProperty({ type: () => PostLike })
  postLike: PostLike;
}
