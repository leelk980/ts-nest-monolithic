import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BaseQueryDto } from 'src/common';
import { Post, PostQueryManager } from 'src/entities';
import { Repository } from 'typeorm';

@Injectable()
export class PostAggQuery {
  private readonly postQuery: PostQueryManager;

  public constructor(
    @InjectRepository(Post)
    private readonly postRepository: Repository<Post>,
  ) {
    this.postQuery = PostQueryManager(this.postRepository);
  }

  async findOnePostById(postId: number) {
    const post = await this.postQuery
      .build()
      .id(postId)
      .leftJoinAndSelect('post.user', 'user')
      .leftJoinAndSelect('post.postCategories', 'postCategory')
      .leftJoinAndSelect('postCategory.category', 'category')
      .leftJoinAndSelect('post.postLikes', 'postLike')
      .leftJoinAndSelect('post.postComments', 'postComment', 'postComment.parentId is null')
      .leftJoinAndSelect('postComment.children', 'postSubComment')
      .addSelect(['post.createdAt', 'post.updatedAt'])
      .getOneOrFail();

    return post;
  }

  async findOneByTitle(title: string) {
    const post = await this.postQuery
      .build()
      .title(title)
      .leftJoinAndSelect('post.postLikes', 'postLike')
      .getOneOrFail();

    return post;
  }

  async findAllPostsByQuery(query: BaseQueryDto) {
    if (!query.joins) {
      query.joins = [{ property: 'post.user', alias: 'user' }];
    }
    const posts = await this.postQuery.manager.findAllByQuery(query);
    return posts;
  }
}
