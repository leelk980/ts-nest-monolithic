import { ApiProperty } from '@nestjs/swagger';
import { BaseOutput } from 'src/common';
import { PostComment } from 'src/entities';

export class PostCommentOutput extends BaseOutput {
  @ApiProperty({ type: () => PostComment })
  postComment: PostComment;
}
