import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { BaseQueryDto, ServiceHelper } from 'src/common';
import { PostExternal } from 'src/routes/@externals';
import { UserAggCommand } from './domains/userAgg.command';
import { UserAggQuery } from './domains/userAgg.query';
import { CreateUserAggDto, UpdateUserAggDto } from './dtos/userAgg.dto';

@Injectable()
export class UserAggService extends ServiceHelper {
  public constructor(
    /** Domains */
    private readonly userAggCommand: UserAggCommand,
    private readonly userAggQuery: UserAggQuery,
    /** Externals */
    private readonly postExternal: PostExternal,
  ) {
    super();
  }

  /** Commands */
  @OnEvent('user.create')
  async createUserAgg(data: CreateUserAggDto) {
    const saga = this.createSaga('createUserAggSaga')
      .setStep({
        name: '유저를 생성한다',
        invoke: async ({ data }: { data: CreateUserAggDto }) => {
          const user = await this.userAggCommand.createUserAgg(data);
          return user;
        },
        compensate: async (params) => console.log(params),
      })
      .setStep({
        name: '웰컴 이메일을 전송한다',
        invoke: async (params) => console.log('welcome ', params),
        compensate: async (params) => console.log(params),
      })
      .build();

    const results = await this.executeSaga(saga, { data });
    return results[0]; // user
  }

  @OnEvent('user.update')
  async updateUserAgg(userId: number, data: UpdateUserAggDto) {
    const saga = this.createSaga('updateUserAggSaga')
      .setStep({
        name: '유저를 업데이트한다',
        invoke: async ({ userId, data }: { userId: number; data: UpdateUserAggDto }) => {
          await this.userAggCommand.updateUserAgg(userId, data);
        },
        compensate: async () => console.log(userId, data),
      })
      .setStep({
        name: '알림을 전송한다',
        invoke: async () => console.log('notification'),
        compensate: async (params) => console.log(params),
      })
      .build();

    await this.executeSaga(saga, { userId, data });
  }

  @OnEvent('user.delete')
  async softDeleteUserAgg(userId: number) {
    const saga = this.createSaga('softDeleteUserAggSaga')
      .setStep({
        name: '유저가 작성한 게시글을 모두 찾는다',
        invoke: async ({ userId }: { userId: number }) => {
          const user = await this.userAggQuery.findOneByUserId(userId);
          const postIds = user.posts.map((post) => post.id);
          return { userId, postIds };
        },
        compensate: async () => console.log(userId),
      })
      .setStep({
        name: '찾은 게시글을 모두 삭제한다',
        invoke: async ({ userId, postIds }: { userId: number; postIds: number[] }) => {
          for await (const postId of postIds) {
            await this.postExternal.softDeletePostAgg(postId); // TODO: 트랜잭션 보장안되서 deleteMany 만들어야함
          }

          return userId;
        },
        compensate: async (params) => console.log(userId, params),
      })
      .setStep({
        name: '유저를 삭제한다',
        invoke: async (userId: number) => {
          await this.userAggCommand.softDeleteUserAgg(userId);
        },
        compensate: async (params) => console.log(userId, params),
      })
      .build();

    await this.executeSaga(saga, { userId });
  }

  /** Queries */
  @OnEvent('user.findOne')
  async findOneUserById(userId: number) {
    const user = await this.userAggQuery.findOneByUserId(userId);
    return user;
  }

  @OnEvent('user.findAll')
  async findAllUserByQuery(query: BaseQueryDto) {
    const users = await this.userAggQuery.findAllUsersByQuery(query);
    return users;
  }
}
