import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { generateCharacters } from 'src/@utils';
import { CommandHelper } from 'src/common';
import { User, UserCategory } from 'src/entities';
import { CommandManager } from 'src/entities';
import { TrxService } from 'src/globals';
import { QueryRunner, Repository } from 'typeorm';
import { CreateUserAggDto, UpdateUserAggDto } from '../dtos/userAgg.dto';

@Injectable()
export class UserAggCommand extends CommandHelper {
  private userCommand: CommandManager<User>;
  private userCategoryCommand: CommandManager<UserCategory>;

  public constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(UserCategory)
    private readonly userCategoryRepository: Repository<UserCategory>,
    protected readonly trxService: TrxService,
  ) {
    super();
  }

  protected registerCommandManagers(queryRunner?: QueryRunner) {
    if (queryRunner) {
      this.userCommand = CommandManager(queryRunner.manager.getRepository(User));
      this.userCategoryCommand = CommandManager(queryRunner.manager.getRepository(UserCategory));
    } else {
      this.userCommand = CommandManager(this.userRepository);
      this.userCategoryCommand = CommandManager(this.userCategoryRepository);
    }
  }

  async createUserAgg(data: CreateUserAggDto) {
    return await this.withTransaction(async () => {
      const { email, name, role, password, userCategories } = data;

      const secretKey = generateCharacters(9);
      const user = await this.userCommand.createOne({
        email,
        name,
        role,
        password: await bcrypt.hash(password, 12),
        secretKey,
      });

      await this.userCategoryCommand.createMany(
        userCategories.map((userCategory) => ({ ...userCategory, userId: user.id })),
      );

      return user;
    });
  }

  async updateUserAgg(userId: number, data: UpdateUserAggDto) {
    return await this.withTransaction(async () => {
      const { name, role } = data;
      await this.userCommand.updateOne(userId, { name, role });
      // if (data.name === '123') {
      //   await this.userCategoryCommand.hardDeleteMany({ userId });
      // }
    });
  }

  async softDeleteUserAgg(userId: number) {
    return await this.withTransaction(async () => {
      return await this.userCommand.softDeleteOne(userId);
    });
  }
}
