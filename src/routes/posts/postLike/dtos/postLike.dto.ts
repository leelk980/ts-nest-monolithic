import { PartialType, PickType } from '@nestjs/swagger';
import { PostLike } from 'src/entities';

export class CreatePostLikeDto extends PickType(PostLike, ['isLiked']) {}

export class UpdatePostLikeDto extends PartialType(CreatePostLikeDto) {}
