export class SagaStep<Input, Output> {
  public name: string;
  private invokeFunction: (params: Input) => Promise<Output>;
  private compensateFunction: (params: Input) => Promise<any>;

  constructor(name: string) {
    this.name = name;
  }

  public setInvocation(method: (params: Input) => Promise<Output>): void {
    this.invokeFunction = method;
  }

  public setCompensation(method: (params: Input) => Promise<any>): void {
    this.compensateFunction = method;
  }

  public async invoke(params?: Input) {
    if (this.invokeFunction) {
      return this.invokeFunction(params);
    }
  }

  public async compensate(params?: Input) {
    if (this.compensateFunction) {
      return this.compensateFunction(params);
    }
  }

  public getName() {
    return this.name;
  }
}
