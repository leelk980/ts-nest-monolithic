export * from './auth/auth.module';
export * from './posts/post.module';
export * from './users/user.module';

export * from './setup/setup.module'; // *import order
