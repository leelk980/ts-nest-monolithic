import PostAggTest from './postAgg';
import PostCommentTest from './postComment';
import PostLikeTest from './postLike';

const PostControllerTest = () => {
  describe('#3-1 PostAgg Test', PostAggTest);
  describe('#3-2 PostComment Test', PostCommentTest);
  describe('#3-3 PostLike Test', PostLikeTest);
};

export default PostControllerTest;
