import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ApiDefinition, BaseQueryDto, CurrUser } from 'src/common';
import { CreatePostAggDto, UpdatePostAggDto } from './dtos/postAgg.dto';
import { PostOutput, PostsOutput } from './dtos/postAgg.output';
import { PostAggService } from './postAgg.service';

@ApiTags('PostAggregate')
@Controller('/posts')
export class PostAggController {
  constructor(private readonly postAggService: PostAggService) {}

  /** Commands */
  @Post('/')
  @ApiDefinition({
    summary: '게시글 생성하기',
    response: PostOutput,
  })
  async createPostAgg(
    @CurrUser('id') userId: number,
    @Body() data: CreatePostAggDto,
  ): Promise<PostOutput> {
    const post = await this.postAggService.createPostAgg(userId, data);
    return { post };
  }

  @Put('/:id')
  @ApiDefinition({
    summary: '게시글 수정하기',
    owner: 'post',
  })
  async updatePostAgg(@Param('id') postId: number, @Body() data: UpdatePostAggDto) {
    await this.postAggService.updatePostAgg(postId, data);
  }

  @Delete('/:id')
  @ApiDefinition({
    summary: '게시글 삭제하기',
    owner: 'post',
  })
  async softDeletePostAggregate(@Param('id') postId: number) {
    await this.postAggService.softDeletePostAgg(postId);
  }

  /** Queries */
  @Get('/:id')
  @ApiDefinition({
    summary: '게시글 하나 가져오기',
    response: PostOutput,
    isPublic: true,
  })
  async findOnePostById(@Param('id') postId: number): Promise<PostOutput> {
    const post = await this.postAggService.findOnePostById(postId);
    return { post };
  }

  @Get('/')
  @ApiDefinition({
    summary: '게시글 목록 가져오기',
    response: PostsOutput,
    isPublic: true,
  })
  async findAllPostsByQuery(@Query() query: BaseQueryDto): Promise<PostsOutput> {
    const posts = await this.postAggService.findAllPostsByQuery(query);
    return { posts };
  }
}
