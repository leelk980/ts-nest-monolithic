import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsIn, IsNumber } from 'class-validator';
import { BaseEntity } from 'src/common';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Category, lastDepthCategoryIds, Post } from '../index';

@Entity()
@Index(['postId', 'categoryId'], { unique: true })
export class PostCategory extends BaseEntity {
  /** Table Columns */
  @ApiProperty({ example: 1 })
  @Column({ type: 'smallint' })
  @IsNumber()
  relevance: number;

  /** Foreign keys */
  @ApiProperty({ example: 1 })
  @Column({ type: 'int' })
  @IsNumber()
  postId: number;

  @ApiProperty({ example: `one of [${lastDepthCategoryIds}]` })
  @Column({ type: 'int' })
  @IsIn(lastDepthCategoryIds)
  categoryId: number;

  /** Relations */
  @ApiPropertyOptional({ example: 'A post which is included this category', type: () => Post }) // *lazy-loading
  @ManyToOne(() => Post, (post) => post.postCategories)
  @JoinColumn()
  post?: Post;

  @ApiPropertyOptional({ example: 'Target category', type: () => Category }) // *lazy-loading
  @ManyToOne(() => Category, (category) => category.postCategories)
  @JoinColumn()
  category?: Category;
}
