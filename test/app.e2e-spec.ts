import { getOneofArrayItem, isTypeOf } from 'src/@utils';
import { lastDepthCategoryIds, UserRoleEnum } from 'src/entities';
import { RedisIoAdapter } from 'src/redis.adaptor';
import { SignUpDto } from 'src/routes/auth/dtos/auth.dto';
import { AuthTokensOutput } from 'src/routes/auth/dtos/auth.output';
import { getConnection } from 'typeorm';
import { HyperTest, initHyperTest, testApp } from './hyperTest';
import AuthControllerTest from './routes/auth';
import PostControllerTest from './routes/posts';
import UserControllerTest from './routes/users';

describe('------ Start E2E Test ------', () => {
  let hyperTest: HyperTest;
  beforeAll(async () => {
    hyperTest = await initHyperTest();
    await setupTest(hyperTest);
  });

  afterAll(async () => {
    await getConnection().dropDatabase();
    await RedisIoAdapter.close();
    await (await testApp).close();
  });

  describe('\n#1 Auth Controller Test', AuthControllerTest);
  describe('\n#2 User Controller Test', UserControllerTest);
  describe('\n#3 Post Controller Test', PostControllerTest);
});

/** Setup */
async function setupTest(hyperTest: HyperTest) {
  await hyperTest.post('/api/setup/categories').authToken().data();

  const { accessToken: adminToken } = await hyperTest
    .post<AuthTokensOutput>('/api/auth/signup')
    .authToken()
    .data(
      isTypeOf<SignUpDto>({
        email: 'admin@admin.com',
        password: '12341234',
        name: 'admin',
        role: UserRoleEnum.Admin,
        userCategories: [{ categoryId: getOneofArrayItem(lastDepthCategoryIds), preference: 1 }],
      }),
    );

  global.adminToken = adminToken;
  global.emptyObj = {};
}
