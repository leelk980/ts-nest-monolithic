import { cloneDeep } from 'lodash';
import { getOneofArrayItem } from 'src/@utils';
import { lastDepthCategoryIds } from 'src/entities';
import { CreatePostAggDto, PostOutput } from 'src/routes/posts/postAgg';
import { createRandomUser, RandomUser } from 'test/@utils';
import { HyperTest, initHyperTest } from 'test/hyperTest';

const tests = () => {
  let randomUser: RandomUser;
  let hyperTest: HyperTest;
  beforeAll(async () => {
    randomUser = await createRandomUser();
    hyperTest = await initHyperTest();
  });

  const validData: CreatePostAggDto = {
    title: 'title',
    content: 'content',
    postCategories: [{ categoryId: getOneofArrayItem(lastDepthCategoryIds), relevance: 1 }],
  };

  it('should fail to create post (invalid token)', async () => {
    const res = await hyperTest
      .post<PostOutput>('/api/posts')
      .authToken('invalid token')
      .data(validData);

    expect(res.isSuccess).toEqual(false);
  });

  it('should fail to create post (without title)', async () => {
    const data = cloneDeep(validData);
    data.title = undefined;

    const res = await hyperTest
      .post<PostOutput>('/api/posts')
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });

  it('should fail to create post (without content)', async () => {
    const data = cloneDeep(validData);
    data.content = undefined;

    const res = await hyperTest
      .post<PostOutput>('/api/posts')
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });

  it('should fail to create post (without category)', async () => {
    const data = cloneDeep(validData);
    data.postCategories = [];

    const res = await hyperTest
      .post<PostOutput>('/api/posts')
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });

  it('should fail to create post (null categoryId)', async () => {
    const data = cloneDeep(validData);
    data.postCategories.push({ categoryId: null, relevance: 1 });

    const res = await hyperTest
      .post<PostOutput>('/api/posts')
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });

  it('should fail to create post (null relevance)', async () => {
    const data = cloneDeep(validData);
    data.postCategories.push({ categoryId: 999, relevance: null });

    const res = await hyperTest
      .post<PostOutput>('/api/posts')
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });

  it('should fail to create post (invalid categoryId)', async () => {
    const data = cloneDeep(validData);
    data.postCategories.push({ categoryId: 999, relevance: 1 });

    const res = await hyperTest
      .post<PostOutput>('/api/posts')
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });

  it('should fail to create post (duplicated category)', async () => {
    const data = cloneDeep(validData);
    data.postCategories.push({ categoryId: 1, relevance: 1 });

    const res = await hyperTest
      .post<PostOutput>('/api/posts')
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(false);
  });

  it('should success to create posts', async () => {
    const res = await hyperTest
      .post<PostOutput>('/api/posts')
      .authToken(randomUser.accessToken)
      .data(validData);

    expect(res.isSuccess).toEqual(true);
  });
};

export default tests;
