import { Saga } from './saga';
import { SagaFlow } from './saga.flow';
import { SagaStep } from './saga.step';

export class SagaBuilder {
  private sagaName: string;
  private currentStep: SagaStep<any, any>;
  private steps: SagaStep<any, any>[] = [];

  public constructor(sagaName: string) {
    this.sagaName = sagaName;
  }

  public setStep<Input = any, Output = any>(params: {
    name: string;
    invoke: (params: Input) => Promise<Output>;
    compensate: (params: Input) => Promise<any>;
  }) {
    const { name, invoke, compensate } = params;
    this.currentStep = new SagaStep<Input, Output>(name);
    this.currentStep.setInvocation(invoke);
    this.currentStep.setCompensation(compensate);

    this.steps.push(this.currentStep);
    return this;
  }

  public build(): Saga {
    const sagaFlow = new SagaFlow(this.steps);
    const saga = new Saga(this.sagaName, sagaFlow);
    return saga;
  }
}
