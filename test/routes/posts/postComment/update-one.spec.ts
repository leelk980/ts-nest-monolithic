import { isTypeOf } from 'src/@utils';
import { Post, PostComment } from 'src/entities';
import { PostOutput } from 'src/routes/posts/postAgg';
import { CreatePostCommentDto, PostCommentOutput } from 'src/routes/posts/postComment';
import { createRandomPost, createRandomUser, RandomUser } from 'test/@utils';
import { HyperTest, initHyperTest } from 'test/hyperTest';

const tests = () => {
  let randomUser: RandomUser;
  let targetPost: Post;
  let targetPostComment: PostComment;
  let hyperTest: HyperTest;
  beforeAll(async () => {
    randomUser = await createRandomUser();
    targetPost = await createRandomPost(randomUser.accessToken);
    hyperTest = await initHyperTest();

    const res = await hyperTest
      .post<PostCommentOutput>(`/api/posts/${targetPost.id}/comments`)
      .authToken(randomUser.accessToken)
      .data(isTypeOf<CreatePostCommentDto>({ content: 'comment' }));

    targetPostComment = res.postComment;
  });

  const data: CreatePostCommentDto = {
    content: 'changed comment',
  };

  it('should success to update comment content', async () => {
    const before = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(before.post.postComments[0].content).not.toEqual(data.content);

    const res = await hyperTest
      .put(`/api/posts/${targetPost.id}/comments/${targetPostComment.id}`)
      .authToken(randomUser.accessToken)
      .data(data);

    expect(res.isSuccess).toEqual(true);

    const after = await hyperTest
      .get<PostOutput>(`/api/posts/${targetPost.id}`)
      .authToken(randomUser.accessToken)
      .query();

    expect(after.post.postComments[0].content).toEqual(data.content);
  });

  it('should fail to change comment`s parentId', async () => {
    const res = await hyperTest
      .put(`/api/posts/${targetPost.id}/${targetPostComment.id}`)
      .authToken(randomUser.accessToken)
      .data({ parentId: 1, ...data });

    expect(res.isSuccess).toEqual(false);
  });
};

export default tests;
