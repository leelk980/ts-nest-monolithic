import { ApiProperty } from '@nestjs/swagger';
import { BaseOutput } from 'src/common';
import { Post } from 'src/entities';

export class PostOutput extends BaseOutput {
  @ApiProperty({ type: () => Post })
  post: Post;
}

export class PostsOutput extends BaseOutput {
  @ApiProperty({ type: () => [Post] })
  posts: Post[];
}
