import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';
import { BaseEntity } from 'src/common';
import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { PostComment } from './postComment.entity';
import { PostCategory, PostLike, User } from '../index';

@Entity()
export class Post extends BaseEntity {
  /** Table Columns */
  @ApiProperty({ example: 'post title' })
  @Column({ type: 'varchar' })
  @IsString()
  title: string;

  @ApiProperty({ example: 'post content' })
  @Column({ type: 'varchar' })
  @IsString()
  content: string;

  /** Foreign keys */
  @ApiProperty({ example: 100 })
  @Column({ type: 'int', nullable: true })
  @IsNumber()
  userId: number;

  /** Relations */
  @ApiPropertyOptional({ example: 'Author information', type: () => User }) // *lazy-loading
  @ManyToOne(() => User, (user) => user.posts)
  @JoinColumn()
  user?: User;

  @ApiPropertyOptional({ example: ['PostComment1', 'PostComment2'], type: () => [PostComment] }) // *lazy-loading
  @OneToMany(() => PostComment, (postComment) => postComment.post)
  postComments?: PostComment[];

  @ApiPropertyOptional({ example: ['PostLike1', 'PostLike2'], type: () => [PostLike] }) // *lazy-loading
  @OneToMany(() => PostLike, (postLike) => postLike.post)
  postLikes?: PostLike[];

  @ApiPropertyOptional({ example: ['PostCategory1', 'PostCategory2'], type: () => [PostCategory] }) // *lazy-loading
  @OneToMany(() => PostCategory, (postCategory) => postCategory.post)
  postCategories?: PostCategory[];
}

/**
 * TypeORM cascade, onDelete 옵션
 * - cascade: OneToMany에서 insert, update 실행시 유효한 옵션.
 *   ex) user.post = posts; user.save(); 하면 post도 생성(혹은 수정)됨
 *
 * - onDelete: ManyToOne에서 부모가 삭제되면 어떻게 동작할지 설정하는 옵션
 *   ex) onDelete: 'CASCADE' user가 삭제되면 user가 가지고 있는 post들도 같이 삭제된다.
 *
 * => Transaction을 일일이 안 걸어도 되서 편하긴 하지만
 *    unexpected한 이슈들이 생길 수 있음.
 * => cascade는 사용하지 않을 예정.
 * => onDelete는 'CASCADE'는 Aggreagation 안에서만 쓰고, Aggregation 밖에서는 따로 처리하는게 좋을듯
 *    +) softDelete에선 동작 안함.
 */
