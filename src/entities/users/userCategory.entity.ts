import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsIn, IsNumber } from 'class-validator';
import { BaseEntity } from 'src/common';
import { Column, Entity, Index, JoinColumn, ManyToOne } from 'typeorm';
import { Category, lastDepthCategoryIds, User } from '../index';

@Entity()
@Index(['userId', 'categoryId'], { unique: true })
export class UserCategory extends BaseEntity {
  /** Table Columns */
  @ApiProperty({ example: 1 })
  @Column({ type: 'smallint' })
  @IsNumber()
  preference: number;

  /** Foreign keys */
  @ApiProperty({ example: 100 })
  @Column({ type: 'int' })
  @IsNumber()
  userId: number;

  @ApiProperty({ example: `one of [${lastDepthCategoryIds}]` })
  @Column({ type: 'int' })
  @IsNumber()
  @IsIn(lastDepthCategoryIds)
  categoryId: number;

  /** Relations */
  @ApiPropertyOptional({ example: 'A user who prefer this category', type: () => User }) // *lazy-loading
  @ManyToOne(() => User, (user) => user.userCategories)
  @JoinColumn()
  user?: User;

  @ApiPropertyOptional({ example: 'Target category', type: () => Category }) // *lazy-loading
  @ManyToOne(() => Category, (category) => category.userCategories)
  @JoinColumn()
  category?: Category;
}
